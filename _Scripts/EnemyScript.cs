﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    private Transform player;
    private void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        navMeshAgent.destination = player.position;
        if (Vector3.Distance(player.position, transform.position) < 2)
        {
            int attackType = Random.Range(1, 2);
            animator.SetTrigger("Attack" + attackType);
        }

    }
}
