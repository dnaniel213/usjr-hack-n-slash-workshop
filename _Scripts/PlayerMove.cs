﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMove : MonoBehaviour
{

    private Rigidbody rb;
    public float moveSpeed, jumpStrength, rotateSpeed;
    private float horizontalMove, verticalMove;
    public Animator anim;
    private bool jumpMove;
    private bool isGrounded;

    private Collider thrust, slash, burst;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    { GetControls(); }



    public void GetControls()
    {
        Slash();
        horizontalMove = CrossPlatformInputManager.GetAxis("Horizontal");
        verticalMove = CrossPlatformInputManager.GetAxis("Vertical");
        jumpMove = CrossPlatformInputManager.GetButton("Jump");
        // Debug.Log("isJumping : "+jumpMove);

        if (CrossPlatformInputManager.GetAxis("Horizontal") != 0 || CrossPlatformInputManager.GetAxis("Vertical") != 0)
        {
            anim.SetBool("isWalking", true);
            rb.MovePosition(transform.position + transform.forward * verticalMove * moveSpeed * Time.deltaTime);
            transform.Rotate(new Vector3(0, horizontalMove * rotateSpeed, 0));

            if (jumpMove && isGrounded)
            {
                Fire();
                isGrounded = false;
                anim.SetBool("isWalking", false);
            }

        }
        //else if (jumpMove && Physics.Raycast(transform.position, new Vector3(0, -1, 0), 2f))
        else if (jumpMove && isGrounded)
        {
            Fire();
            isGrounded = false;
            anim.SetBool("isWalking", false);
        }
        else
        {
            anim.SetBool("isWalking", false);

        }

    }





    public void Fire()
    {
        rb.AddForce(Vector3.up * jumpStrength, ForceMode.Impulse);
        anim.SetTrigger("Jump");

    }
    private void OnTriggerEnter(Collider other)
    {
        isGrounded = true;
    }

    // private void OnTriggerExit(Collider other) {
    //     isGrounded = false;
    // }

    public void Slash()
    {

    }

    public void Burst()
    {


    }

    public void Thrust()
    {

    }



}
