﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLose : MonoBehaviour
{
    public bool win;
    public bool lose;
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (win == true)
            {
                other.gameObject.GetComponent<PlayerMove>().anim.SetTrigger("Win");
                other.gameObject.GetComponent<PlayerMove>().anim.SetBool("isWalking", false); ;

                other.gameObject.GetComponent<PlayerMove>().enabled = false;

                StartCoroutine(WinDelay());
            }
            else if (lose == true)
            {
                other.gameObject.GetComponent<PlayerMove>().anim.SetTrigger("Lose");
                other.gameObject.GetComponent<PlayerMove>().anim.SetBool("isWalking", false); ;
                other.gameObject.GetComponent<PlayerMove>().enabled = false;
                StartCoroutine(LoseDelay());
            }
        }
    }

    private IEnumerator LoseDelay()
    {
        yield return new WaitForSeconds(2.0f);

        GameUI.LoseGame();
    }

    private IEnumerator WinDelay()
    {
        yield return new WaitForSeconds(2.0f);

        GameUI.WinGame();
    }
}
