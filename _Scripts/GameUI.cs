﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{

    public GameObject mainMenu, winMenu, loseMenu;
    public Text gameTitle;
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 0;
        gameTitle.text = SceneManager.GetActiveScene().name;
        mainMenu.SetActive(true);
    }

    public void BeginGame()
    {
        Time.timeScale = 1;
        mainMenu.SetActive(false);

    }


    public static void WinGame()
    {
        Time.timeScale = 0;
        GameObject.Find("Canvas").GetComponent<GameUI>().winMenu.SetActive(true);
    }

    public static void LoseGame()
    {
        Time.timeScale = 0;
        GameObject.Find("Canvas").GetComponent<GameUI>().loseMenu.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

